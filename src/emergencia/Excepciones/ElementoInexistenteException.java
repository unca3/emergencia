/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.Excepciones;
/**
 *
 * @author Julian
 */
public class ElementoInexistenteException extends Exception{
    public ElementoInexistenteException(String message) {
        super(message);
    }
}
