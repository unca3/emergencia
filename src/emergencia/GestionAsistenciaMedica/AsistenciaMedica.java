/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAsistenciaMedica;
import emergencia.GestionAfiliado.Afiliado;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author Julian
 */
public class AsistenciaMedica {
    private LocalDate fecha;
    private LocalTime hora;
    private String tipoAtencion;
    private String diagnostico;
    private String receta;
    private String estadoPaciente;
    
    private SolicitudEmergencia solicitudEmergencia;
    private Afiliado afiliado;

    public AsistenciaMedica(LocalDate fecha, LocalTime hora, String tipoAtencion, String diagnostico, String receta, String estadoPaciente) {
        this.fecha = fecha;
        this.hora = hora;
        this.tipoAtencion = tipoAtencion;
        this.diagnostico = diagnostico;
        this.receta = receta;
        this.estadoPaciente = estadoPaciente;
    }

    public LocalDate getFecha() {
        return fecha;
    }
    public LocalTime getHora() {
        return hora;
    }
    public String getTipoAtencion() {
        return tipoAtencion;
    }
    public String getDiagnostico() {
        return diagnostico;
    }
    public String getReceta() {
        return receta;
    }
    public String getEstadoPaciente() {
        return estadoPaciente;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    public void setHora(LocalTime hora) {
        this.hora = hora;
    }
    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }
    public void setDiagnostico(String diagnostico) {
        this.diagnostico = diagnostico;
    }
    public void setReceta(String receta) {
        this.receta = receta;
    }
    public void setEstadoPaciente(String estadoPaciente) {
        this.estadoPaciente = estadoPaciente;
    }

    public SolicitudEmergencia getSolicitudEmergencia() {
        return solicitudEmergencia;
    }
    public void setSolicitudEmergencia(SolicitudEmergencia solicitudEmergencia) {
        this.solicitudEmergencia = solicitudEmergencia;
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()){
            return false;
        }
        AsistenciaMedica asistencia = (AsistenciaMedica)obj;
        return (asistencia.hora.equals(this.hora) && asistencia.fecha.equals(this.fecha) && asistencia.afiliado.getNroDocumento() == this.afiliado.getNroDocumento());
    }
}
