/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAsistenciaMedica;
import emergencia.GestionEmpleados.Chofer;
import emergencia.GestionEmpleados.Doctor;
import emergencia.GestionEmpleados.Enfermero;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Julian
 */
public class SolicitudEmergencia {
    private Doctor doctor;
    private List<Enfermero> enfermeros;
    private Chofer chofer;
    private AsistenciaMedica asistenciaMedica;
    private SolicitudAsistencia solicitudAsistencia;

    public SolicitudEmergencia() {
        enfermeros = new ArrayList<>();
    }

    public Chofer getChofer() {
        return chofer;
    }
    public AsistenciaMedica getAsistenciaMedica() {
        return asistenciaMedica;
    }
    public SolicitudAsistencia getSolicitudAsistencia() {
        return solicitudAsistencia;
    }
    public Doctor getDoctor() {
        return doctor;
    }

    public void setChofer(Chofer chofer) {
        this.chofer = chofer;
    }
    public void setAsistenciaMedica(AsistenciaMedica asistenciaMedica) {
        this.asistenciaMedica = asistenciaMedica;
    }
    public void setSolicitudAsistencia(SolicitudAsistencia solicitudAsistencia) {
        this.solicitudAsistencia = solicitudAsistencia;
    }
    public void setDoctor(Doctor doctor) {
        this.doctor = doctor;
    }
    
    public void agregarEnfermero(Enfermero enfermero) {
        enfermeros.add(enfermero);
        System.out.println("Enfermero agregado exitosamente");
    }
    public void eliminarEnfermero(Enfermero enfermero) {
        enfermeros.remove(enfermero);
        System.out.println("Enfermero eliminado exitosamente");
    }
    public void mostrarLista() {
        System.out.println("Lista de enfermeros:");        
        for(Enfermero enfe : enfermeros) {
            System.out.println(enfe);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SolicitudEmergencia solicitudEmer = (SolicitudEmergencia)obj;
        return (solicitudEmer.solicitudAsistencia.equals(this.solicitudAsistencia));
    }
}
