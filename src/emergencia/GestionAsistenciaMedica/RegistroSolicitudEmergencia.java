/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAsistenciaMedica;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroSolicitudEmergencia {
    private final List<SolicitudEmergencia> solicitudesEmergencia = new ArrayList<>();

    public void darAlta(SolicitudEmergencia solicitudEmergencia) throws ElementoDuplicadoException{
        if(solicitudesEmergencia.contains(solicitudEmergencia)) {
            throw new ElementoDuplicadoException("La solicitud de emergencia ya se encuentra en la lista");            
        } else {
            solicitudesEmergencia.add(solicitudEmergencia);
            System.out.println("La solicitud de emergencia se dio de alta exitosamente");
        }        
    }
    public SolicitudEmergencia buscarSolicitud(SolicitudEmergencia solicitudBuscada) throws ElementoInexistenteException{
        for(SolicitudEmergencia soli : solicitudesEmergencia) {
            if(soli.equals(solicitudBuscada) == true) {
                System.out.println("Se encontro la solicitud de emergencia buscada");
                return soli;
            }
        }
        throw new ElementoInexistenteException("No se encontro la solicitud de emergencia buscada");
    }
    public void modificarSolicitud(SolicitudEmergencia solicitudBuscada, SolicitudEmergencia solicitudActualizada) {
        try {
            int indice = solicitudesEmergencia.indexOf(this.buscarSolicitud(solicitudBuscada));
            solicitudesEmergencia.set(indice, solicitudActualizada);
            System.out.println("La solicitud de emergencia ha sido modificada exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void darBaja(SolicitudEmergencia solicitudEmergencia) throws ElementoDuplicadoException{
        if(solicitudesEmergencia.contains(solicitudEmergencia)) {
            solicitudesEmergencia.remove(solicitudEmergencia);
            System.out.println("La solicitud de emergencia se dio de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("La solicitud de emergencia se encuentra en la lista");            
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de solicitudes de emergencia:");        
        for(SolicitudEmergencia soli : solicitudesEmergencia) {
            System.out.println(soli);
        }
    }
}
