/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAsistenciaMedica;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroSolicitudAsistencia {
    private final List<SolicitudAsistencia> listaSolicitudes = new ArrayList<>();

    public void darAlta(SolicitudAsistencia solicitudAsistencia) throws ElementoDuplicadoException{
        if(listaSolicitudes.contains(solicitudAsistencia)) {
            throw new ElementoDuplicadoException("La solicitud de asistencia ya se encuentra en la lista");            
        } else {
            listaSolicitudes.add(solicitudAsistencia);
            System.out.println("La solicitud de asistencia se dio de alta exitosamente");
        }        
    }
    public SolicitudAsistencia buscarSolicitud(SolicitudAsistencia solicitudBuscada) throws ElementoInexistenteException{
        for(SolicitudAsistencia soli : listaSolicitudes) {
            if(soli.equals(solicitudBuscada) == true) {
                System.out.println("Se encontro la solicitud de asistencia buscada");
                return soli;
            }
        }
        throw new ElementoInexistenteException("No se encontro la solicitud de asistencia buscada");
    }
    public void modificarSolicitud(SolicitudAsistencia solicitudBuscada, SolicitudAsistencia solicitudActualizada) {
        try {
            int indice = listaSolicitudes.indexOf(this.buscarSolicitud(solicitudBuscada));
            listaSolicitudes.set(indice, solicitudActualizada);
            System.out.println("La solicitud de asistencia ha sido modificada exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void darBaja(SolicitudAsistencia solicitudAsistencia) throws ElementoDuplicadoException{
        if(listaSolicitudes.contains(solicitudAsistencia)) {
            listaSolicitudes.remove(solicitudAsistencia);
            System.out.println("La solicitud de asistencia se dio de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("La solicitud de asistenciano se encuentra en la lista");
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de solicitudes de asistencia:");        
        for(SolicitudAsistencia soli : listaSolicitudes) {
            System.out.println(soli);
        }
    }
}