/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAsistenciaMedica;
import emergencia.GestionAfiliado.Afiliado;
import emergencia.GestionEmpleados.Administrativo;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author Julian
 */
public class SolicitudAsistencia {
    private String nombreSolicitante;
    private Integer numeroDocumento;
    private String correo;
    private String direccion;
    private String problemaMedico;
    private String tipoAtencion;
    private String direccionActual;
    private LocalDate fecha;
    private LocalTime hora;
//    private Double pago;
//    private Double mora;
    
    private Administrativo administrativo;
    private Afiliado afiliado;
    private SolicitudEmergencia solicitudEmergencia;

    public SolicitudAsistencia(String nombreSolicitante, Integer numeroDocumento, String correo, String direccion, String problemaMedico, String tipoAtencion, String direccionActual, LocalDate fecha, LocalTime hora, Double pago, Double mora) {
        this.nombreSolicitante = nombreSolicitante;
        this.numeroDocumento = numeroDocumento;
        this.correo = correo;
        this.direccion = direccion;
        this.problemaMedico = problemaMedico;
        this.tipoAtencion = tipoAtencion;
        this.direccionActual = direccionActual;
        this.fecha = fecha;
        this.hora = hora;
//        this.pago = pago;
//        this.mora = mora;
    }
    
    public String getNombreSolicitante() {
        return nombreSolicitante;
    }
    public Integer getNumeroDocumento() {
        return numeroDocumento;
    }
    public String getCorreo() {
        return correo;
    }
    public String getDireccion() {
        return direccion;
    }
    public String getProblemaMedico() {
        return problemaMedico;
    }
    public String getTipoAtencion() {
        return tipoAtencion;
    }
    public String getDireccionActual() {
        return direccionActual;
    }
    public LocalDate getFecha() {
        return fecha;
    }
    public LocalTime getHora() {
        return hora;
    }
//    public Double getPago() {
//        return pago;
//    }
//    public Double getMora() {
//        return mora;
//    }

    public void setNombreSolicitante(String nombreSolicitante) {
        this.nombreSolicitante = nombreSolicitante;
    }
    public void setNumeroDocumento(Integer numeroDocumento) {
        this.numeroDocumento = numeroDocumento;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }
    public void setProblemaMedico(String problemaMedico) {
        this.problemaMedico = problemaMedico;
    }
    public void setTipoAtencion(String tipoAtencion) {
        this.tipoAtencion = tipoAtencion;
    }
    public void setDireccionActual(String direccionActual) {
        this.direccionActual = direccionActual;
    }
    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }
    public void setHora(LocalTime hora) {
        this.hora = hora;
    }
//    public void setPago(Double pago) {
//        this.pago = pago;
//    }
//    public void setMora(Double mora) {
//        this.mora = mora;
//    }

    public Administrativo getAdministrativo() {
        return administrativo;
    }
    public Afiliado getAfiliado() {
        return afiliado;
    }
    public SolicitudEmergencia getSolicitudEmergencia() {
        return solicitudEmergencia;
    }

    public void setAdministrativo(Administrativo administrativo) {
        this.administrativo = administrativo;
    }
    public void setAfiliado(Afiliado afiliado) {
        this.afiliado = afiliado;
    }
    public void setSolicitudEmergencia(SolicitudEmergencia solicitudEmergencia) {
        this.solicitudEmergencia = solicitudEmergencia;
    }    
    
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        SolicitudAsistencia solicitud = (SolicitudAsistencia)obj;
        return (solicitud.direccionActual.equals(this.direccionActual) && solicitud.fecha.equals(this.fecha) && solicitud.hora.equals(this.hora) && solicitud.numeroDocumento.equals(this.numeroDocumento));
    }
}
