/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAsistenciaMedica;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroAsistenciaMedica {
    private final List<AsistenciaMedica> listaAsistencias = new ArrayList<>();

    public void darAlta(AsistenciaMedica asistenciaMedica) throws ElementoDuplicadoException{
        if(listaAsistencias.contains(asistenciaMedica)) {
            throw new ElementoDuplicadoException("La asistencia medica ya se encuentra en la lista");
        } else {
            listaAsistencias.add(asistenciaMedica);
            System.out.println("La asistencia medica se dio de alta exitosamente");
        }        
    }
    public AsistenciaMedica buscarAsistencia(AsistenciaMedica asistenciaBuscada) throws ElementoInexistenteException{
        for(AsistenciaMedica asis : listaAsistencias) {
            if(asis.equals(asistenciaBuscada) == true) {
                System.out.println("Se encontro la asistencia medica buscada");
                return asis;
            }
        }
        throw new ElementoInexistenteException("No se encontro la asistencia medica buscada");
    }
    public void modificarAsistencia(AsistenciaMedica asistenciaBuscada, AsistenciaMedica asistenciaActualizada) {
        try {
            int indice = listaAsistencias.indexOf(this.buscarAsistencia(asistenciaBuscada));
            listaAsistencias.set(indice, asistenciaActualizada);
            System.out.println("La asistencia medica ha sido modificada exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void darBaja(AsistenciaMedica asistenciaMedica) throws ElementoDuplicadoException{
        if(listaAsistencias.contains(asistenciaMedica)) {
            listaAsistencias.remove(asistenciaMedica);
            System.out.println("La asistencia medica se dio de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("La asistencia medica se encuentra en la lista");
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de asistencias medicas:");        
        for(AsistenciaMedica asis : listaAsistencias) {
            System.out.println(asis);
        }
    }    
}
