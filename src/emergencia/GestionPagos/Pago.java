/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionPagos;
import emergencia.GestionAfiliado.Afiliado;
import emergencia.GestionAsistenciaMedica.SolicitudAsistencia;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class Pago {
    private LocalDate fecha;
    private LocalTime hora;
    private String numeroFactura;
    private String tipoFactura;
    private double porcentajeIva;
    private double tarifaPorAfiliado;
    private double tarifaPorFamiliar;
    private Afiliado titular;
    private SolicitudAsistencia solicitudAsistencia; //deberia ser a asistencia medica?

    private double montoMensual;
    private double montoTotal;
    private double cantidadPagada;
    private double mora;
    //Declaracion de listas
    private List<Item> servicios;

    public class Item{
        //Pago de servicios
        private String servicio;
        private double costoServicio;
        private int cantidadServicio;
        private double montoTotalServicio;
    
        public Item(String servicio, double costoServicio, int cantidadServicio) {
            this.servicio = servicio;
            this.costoServicio = costoServicio;
            this.cantidadServicio = cantidadServicio;
        }
        public void calcularServicio() {
            this.montoTotalServicio = this.costoServicio * this.cantidadServicio;
        }
        public double getMontoTotalServicio() {
            return montoTotalServicio;
        }
    }
    
    public Pago(LocalDate fecha, LocalTime hora, String numeroFactura, String tipoFactura, Afiliado titular,/*, Double cantidadPagada,*/ double porcentajeIva, double tarifaPorAfiliado, double tarifaPorFamiliar) {
        this.fecha = fecha;
        this.hora = hora;
        this.numeroFactura = numeroFactura;
        this.tipoFactura = tipoFactura;
        this.titular = titular;
//        this.cantidadPagada = cantidadPagada;
        this.porcentajeIva = porcentajeIva;
        this.tarifaPorAfiliado = tarifaPorAfiliado;
        this.tarifaPorFamiliar = tarifaPorFamiliar;
        servicios = new ArrayList<>();
    }
    
    public void agregarServicio(String servicio, double costoServicio, int cantidadServicio) {
        Item ser = new Item(servicio, costoServicio, cantidadServicio);
        servicios.add(ser);
    }
    
    public void calcularMensualidad() {
        int cantidadFamiliares = titular.getCantidadFamiliares();
        this.montoMensual = this.tarifaPorAfiliado + (this.tarifaPorFamiliar * cantidadFamiliares);
    }
    public void calcularMontoTotalMes() {
        double montoServicio = 0;
        calcularMensualidad();
        for(Item ser : servicios) {
            montoServicio = montoServicio + ser.montoTotalServicio;
        }
        this.montoTotal = this.montoMensual + montoServicio;
    }

    public LocalDate getFecha() {
        return fecha;
    }
    public LocalTime getHora() {
        return hora;
    }
    public String getNumeroFactura() {
        return numeroFactura;
    }
    public String getTipoFactura() {
        return tipoFactura;
    }
    public double getPorcentajeIva() {
        return porcentajeIva;
    }
    public double getTarifaPorAfiliado() {
        return tarifaPorAfiliado;
    }
    public double getTarifaPorFamiliar() {
        return tarifaPorFamiliar;
    }

    public Afiliado getTitular() {
        return titular;
    }
    
    @Override
    public boolean equals(Object obj){
        if (this == obj) {
            return true;
        }
        if (obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Pago pago = (Pago)obj;
        return (pago.numeroFactura.equals(this.numeroFactura) && pago.fecha.equals(this.fecha));
    }
    @Override
    public String toString() {
        return "Pago{" + "fecha=" + fecha + ", hora=" + hora + ", numeroFactura=" + numeroFactura + ", tipoFactura=" + tipoFactura + ", porcentajeIva=" + porcentajeIva + ", tarifaPorAfiliado=" + tarifaPorAfiliado + ", tarifaPorFamiliar=" + tarifaPorFamiliar + '}';
    }
}