/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionPagos;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroPagos {
    private final List<Pago> comprobantes = new ArrayList<>();
    
    public void agregarPago(Pago pagoAgregado) throws ElementoDuplicadoException{
        if(comprobantes.contains(pagoAgregado)) {
            throw new ElementoDuplicadoException("El comprobante de pago ya se encuentra en la lista");
        } else {
            comprobantes.add(pagoAgregado);
            System.out.println("El comprobante de pago fue agregado exitosamente");
        }
    }
    public Pago buscarPago(Pago pagoBuscado) throws ElementoInexistenteException{
        for(Pago pago : comprobantes) {
            if(pago.equals(pagoBuscado) == true) {
                System.out.println("Se encontro el comprobante de pago buscado");
                return pago;
            }
        }
        throw new ElementoInexistenteException("No se encontro el comprobante de pago buscado");
    }
    public ArrayList buscarTodo() {
        return (ArrayList) comprobantes;
    }
    public void modificarPago(Pago pagoBuscado, Pago pagoActualizado) throws ElementoInexistenteException{
        int indice = comprobantes.indexOf(pagoBuscado);
        if(indice == -1) {            
            throw new ElementoInexistenteException("El comprobante de pago no se encuentra en la lista");
        } else {
            System.out.println("Numero de elementos: "+ indice);
            comprobantes.set(indice, pagoActualizado);
            comprobantes.remove(pagoBuscado);
            System.out.println("El comprobante de pago ha sido modificado exitosamente");            
        }
    }
    public void eliminarPago(Pago pagoEliminado) throws ElementoInexistenteException{
        if(comprobantes.contains(pagoEliminado)) {
            comprobantes.remove(pagoEliminado);
            System.out.println("El comprobante de pago ha sido eliminado exitosamente");
        } else {
            throw new ElementoInexistenteException("El comprobante de pago no se encuentra en la lista");
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de comprobantes:");        
        for(Pago pago : comprobantes) {
            System.out.println(pago);
        }
    }
}