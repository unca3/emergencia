/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package emergencia;
import emergencia.GestionAfiliado.Afiliado;
import emergencia.GestionAfiliado.Familiar;
import emergencia.GestionAfiliado.RegistroFamiliar;
import emergencia.GestionPagos.Pago;
import emergencia.Interfaz.InterfazUsuario;
import java.time.*;
/**
 *
 * @author Usuario
 */
public class Principal {
    /**
     * @param args the command line arguments
//     */
    public static void main(String[] args) {
//        Emergencia emergencia = Emergencia.instancia();
        InterfazUsuario app = new InterfazUsuario();
        app.setVisible(true);
        
        // Pruebas de gestion de afiliados
//        Afiliado afi = new Afiliado("ju@gmail.com", "111-222", LocalDate.of(2001, 7, 10),"Medina Julian", "21333222", "M", 22, "casa2", LocalDate.of(2023, 10, 5));
//        Afiliado afi1 = new Afiliado("ca@gmail.com", "111-122", LocalDate.of(2008, 10, 5),"Carlos Daniel", "11333222", "M", 22, "casa2", LocalDate.of(2023, 10, 5));
//        
//        RegistroFamiliar familiares = new RegistroFamiliar();
//        
//        afi.setRegistroFamiliar(familiares);
//        
//        Familiar fami = new Familiar("Carlos A", "111", "M",15,"casa2", LocalDate.of(2008,2,6), afi);
//        Familiar fami1 = new Familiar("Juan P", "111", "M",14,"casa2", LocalDate.of(2009,2,6), afi);
//        Familiar fami2 = new Familiar("Uriel A", "111", "M",16,"casa2", LocalDate.of(2007,2,6), afi);
        //Familiar agregado 
//        familiares.darAlta(fami);
//        familiares.darAlta(fami1);
//        //Familiar agregado repetido
//        familiares.darAlta(fami1);
//        familiares.darAlta(fami2);
        
//        afi.getRegistroFamiliar();
//        
//        //Familiar eliminado
//        familiares.darBaja(fami2);
//        //Familiar inexistente eliminado
//        familiares.darBaja(fami2);
//
//        afi.getRegistroFamiliar();
//        
//        //Familiar modificado inexistente
//        familiares.modificarFamiliar(fami2, fami2);
//        //Familiar modificado
//        familiares.modificarFamiliar(fami1, fami2);
//
//        afi.getRegistroFamiliar();
        
        //Pago
//        Pago pago1 = new Pago(LocalDate.of(2023, 1, 1), LocalTime.of(0, 0),"111-11", "Tipo c", afi,20, 5000,3000);
//        pago1.calcularMensualidad();
    }
}