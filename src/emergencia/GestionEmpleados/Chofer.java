/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionEmpleados;
import emergencia.GestionAsistenciaMedica.SolicitudEmergencia;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Julian
 */
public class Chofer extends Empleado{
    private Integer numeroLicencia;
    private String claseLicencia;
    private LocalDate vencimientoLicencia;
    private String ruta;
    private LocalTime horasConduccion;
    private LocalTime horasExtra;

    private final List<Chofer> listaChoferes= new ArrayList<>();

    public Chofer(Integer numeroLicencia, String claseLicencia, LocalDate vencimientoLicencia, String ruta, LocalTime horasConduccion, LocalTime horasExtra, LocalDate fechaContratacion, Integer añosExperiencia, LocalTime horarioTrabajo, Double salario, String beneficios, LocalTime horasTrabajadas, String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        super(fechaContratacion, añosExperiencia, horarioTrabajo, salario, beneficios, horasTrabajadas, nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.numeroLicencia = numeroLicencia;
        this.claseLicencia = claseLicencia;
        this.vencimientoLicencia = vencimientoLicencia;
        this.ruta = ruta;
        this.horasConduccion = horasConduccion;
        this.horasExtra = horasExtra;
    }

    public Integer getNumeroLicencia() {
        return numeroLicencia;
    }
    public void setNumeroLicencia(Integer numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }
    public String getClaseLicencia() {
        return claseLicencia;
    }
    public void setClaseLicencia(String claseLicencia) {
        this.claseLicencia = claseLicencia;
    }
    public LocalDate getVencimientoLicencia() {
        return vencimientoLicencia;
    }
    public void setVencimientoLicencia(LocalDate vencimientoLicencia) {
        this.vencimientoLicencia = vencimientoLicencia;
    }
    public String getRuta() {
        return ruta;
    }
    public void setRuta(String ruta) {
        this.ruta = ruta;
    }
    public LocalTime getHorasConduccion() {
        return horasConduccion;
    }
    public void setHorasConduccion(LocalTime horasConduccion) {
        this.horasConduccion = horasConduccion;
    }
    public LocalTime getHorasExtra() {
        return horasExtra;
    }
    public void setHorasExtra(LocalTime horasExtra) {
        this.horasExtra = horasExtra;
    }
    
    
    public void darAlta(Chofer chofer) throws ElementoDuplicadoException{
        if(listaChoferes.contains(chofer)) {
            throw new ElementoDuplicadoException("El chofer ya se encuentra en la lista");            
        } else {
            listaChoferes.add(chofer);
            System.out.println("Datos del chofer de alta exitosamente");
        }
    }
    
    public Chofer buscarChofer(Chofer choferBuscar) throws ElementoInexistenteException{
        for(Chofer chofer : listaChoferes) {
            if(chofer.equals(choferBuscar) == true) {
                System.out.println("Se encontro el chofer buscado");
                return chofer;
            }
        }
        throw new ElementoInexistenteException("No se encontro el chofer buscado");
    }
    
    public void modificarChofer(Chofer choferBuscar, Chofer choferActualizado) {
        try {
            int indice = listaChoferes.indexOf(this.buscarChofer(choferBuscar));
            listaChoferes.set(indice, choferActualizado);
            System.out.println("Los datos del chofer fue actualizado exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
     
    public void darBaja(Chofer chofer) throws ElementoDuplicadoException{
        if(listaChoferes.contains(chofer)) {
            listaChoferes.remove(chofer);
            System.out.println("Datos del chofer dado de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("Los datos del chofer no se encuentra en la lista");            
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de choferes:");        
        for(Chofer conductor : listaChoferes) {
            System.out.println(conductor);
        }
    }
    
    
    
     @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Chofer chofer = (Chofer) obj;
        return chofer.numeroLicencia.equals(this.numeroLicencia) && chofer.claseLicencia.equals(this.claseLicencia);
    }
}
