/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionEmpleados;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class Enfermero extends Empleado{
    private String titulo;
    private LocalDate fechaGraduacion;
    private String numeroLicencia;
    private String turnoGuardia;
    private LocalTime horasGuardia;
    private String pacienteAsignado;
    private String sectorAsignado;
    
    private final List<Enfermero> listaEnfermero = new ArrayList<>();

    public Enfermero(String titulo, LocalDate fechaGraduacion, String numeroLicencia, String turnoGuardia, LocalTime horasGuardia, String pacienteAsignado, String sectorAsignado, LocalDate fechaContratacion, Integer añosExperiencia, LocalTime horarioTrabajo, Double salario, String beneficios, LocalTime horasTrabajadas, String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        super(fechaContratacion, añosExperiencia, horarioTrabajo, salario, beneficios, horasTrabajadas, nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.titulo = titulo;
        this.fechaGraduacion = fechaGraduacion;
        this.numeroLicencia = numeroLicencia;
        this.turnoGuardia = turnoGuardia;
        this.horasGuardia = horasGuardia;
        this.pacienteAsignado = pacienteAsignado;
        this.sectorAsignado = sectorAsignado;
    }    

    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public LocalDate getFechaGraduacion() {
        return fechaGraduacion;
    }
    public void setFechaGraduacion(LocalDate fechaGraduacion) {
        this.fechaGraduacion = fechaGraduacion;
    }
    public String getNumeroLicencia() {
        return numeroLicencia;
    }
    public void setNumeroLicencia(String numeroLicencia) {
        this.numeroLicencia = numeroLicencia;
    }
    public String getTurnoGuardia() {
        return turnoGuardia;
    }
    public void setTurnoGuardia(String turnoGuardia) {
        this.turnoGuardia = turnoGuardia;
    }
    public LocalTime getHorasGuardia() {
        return horasGuardia;
    }
    public void setHorasGuardia(LocalTime horasGuardia) {
        this.horasGuardia = horasGuardia;
    }
    public String getPacienteAsignado() {
        return pacienteAsignado;
    }
    public void setPacienteAsignado(String pacienteAsignado) {
        this.pacienteAsignado = pacienteAsignado;
    }
    public String getSectorAsignado() {
        return sectorAsignado;
    }
    public void setSectorAsignado(String sectorAsignado) {
        this.sectorAsignado = sectorAsignado;
    }
    
    public void darAlta(Enfermero enfermero) throws ElementoDuplicadoException{
        if(listaEnfermero.contains(enfermero)) {
            throw new ElementoDuplicadoException("El enfermero ya se encuentra en la lista");            
        } else {
            listaEnfermero.add(enfermero);
            System.out.println("Los datos del enfermero dado de alta exitosamente");
        }
    }
    
    public Enfermero buscarEnfermero(Enfermero enferBuscar) throws ElementoInexistenteException{
        for(Enfermero enfermero : listaEnfermero) {
            if(enfermero.equals(enferBuscar) == true) {
                System.out.println("Se encontro el enfermero buscado");
                return enfermero;
            }
        }
        throw new ElementoInexistenteException("No se encontro los datos buscado");
    }
    
    public void modificarEnfermero(Enfermero enferBuscar, Enfermero enferActualizar) {
        try {
            int indice = listaEnfermero.indexOf(this.buscarEnfermero(enferBuscar));
            listaEnfermero.set(indice, enferActualizar);
            System.out.println("Los datos del enfermero actualizados exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
     
    public void darBaja(Enfermero enfermero) throws ElementoDuplicadoException{
        if(listaEnfermero.contains(enfermero)) {
            listaEnfermero.remove(enfermero);
            System.out.println("Enfermero dado de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("El enfermero no se encuentra en la lista");            
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de enfermeros actualizada:");        
        for(Enfermero enfermero : listaEnfermero) {
            System.out.println(enfermero);
        }
   }
    
     @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Enfermero enfermero = (Enfermero) obj;
        return enfermero.numeroLicencia.equals(this.numeroLicencia) && enfermero.titulo.equals(this.titulo);
    }    
}