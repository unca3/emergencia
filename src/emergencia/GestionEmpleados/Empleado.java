/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionEmpleados;
import emergencia.GestionAfiliado.Persona;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 *
 * @author Julian
 */
public class Empleado extends Persona{
    private LocalDate fechaContratacion;
    private Integer añosExperiencia;
    private LocalTime horarioTrabajo;
    private Double salario;
    private String beneficios;
    private LocalTime horasTrabajadas;
    

    public Empleado(LocalDate fechaContratacion, Integer añosExperiencia, LocalTime horarioTrabajo, Double salario, String beneficios, LocalTime horasTrabajadas, String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        super(nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.fechaContratacion = fechaContratacion;
        this.añosExperiencia = añosExperiencia;
        this.horarioTrabajo = horarioTrabajo;
        this.salario = salario;
        this.beneficios = beneficios;
        this.horasTrabajadas = horasTrabajadas;
    }
    
    public LocalDate getFechaContratacion() {
        return fechaContratacion;
    }
    public Integer getAñosExperiencia() {
        return añosExperiencia;
    }
    public LocalTime getHorarioTrabajo() {
        return horarioTrabajo;
    }
    public Double getSalario() {
        return salario;
    }
    public String getBeneficios() {
        return beneficios;
    }
    public LocalTime getHorasTrabajadas() {
        return horasTrabajadas;
    }

    public void setFechaContratacion(LocalDate fechaContratacion) {
        this.fechaContratacion = fechaContratacion;
    }
    public void setAñosExperiencia(Integer añosExperiencia) {
        this.añosExperiencia = añosExperiencia;
    }
    public void setHorarioTrabajo(LocalTime horarioTrabajo) {
        this.horarioTrabajo = horarioTrabajo;
    }
    public void setSalario(Double salario) {
        this.salario = salario;
    }
    public void setBeneficios(String beneficios) {
        this.beneficios = beneficios;
    }
    public void setHorasTrabajadas(LocalTime horasTrabajadas) {
        this.horasTrabajadas = horasTrabajadas;
    }

    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Empleado empleado = (Empleado)obj;
        return empleado.getNroDocumento().equals(this.getNroDocumento()) && empleado.getNombreApellido().equals(this.getNombreApellido()) && empleado.fechaContratacion.equals(this.fechaContratacion);
    }
}
