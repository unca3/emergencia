/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionEmpleados;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class Doctor extends Empleado{
    private String titulo;
    private LocalDate fechaGraduacion;
    private String numeroLicenciaMedica;
    private String especialidad;
    private String sector;
    private String turnoGuardia;
    private LocalTime horasGuardia;
    private String pacienteAsignado;
    private String tratamiento;
    private String receta;

    private final List<Doctor> listaDoctor = new ArrayList<>();
 
    public Doctor(String titulo, LocalDate fechaGraduacion, String numeroLicenciaMedica, String especialidad, String sector, String turnoGuardia, LocalTime horasGuardia, String pacienteAsignado, String tratamiento, String receta, LocalDate fechaContratacion, Integer añosExperiencia, LocalTime horarioTrabajo, Double salario, String beneficios, LocalTime horasTrabajadas, String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        super(fechaContratacion, añosExperiencia, horarioTrabajo, salario, beneficios, horasTrabajadas, nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.titulo = titulo;
        this.fechaGraduacion = fechaGraduacion;
        this.numeroLicenciaMedica = numeroLicenciaMedica;
        this.especialidad = especialidad;
        this.sector = sector;
        this.turnoGuardia = turnoGuardia;
        this.horasGuardia = horasGuardia;
        this.pacienteAsignado = pacienteAsignado;
        this.tratamiento = tratamiento;
        this.receta = receta;
    }

    public String getTitulo() {
        return titulo;
    }
    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }
    public LocalDate getFechaGraduacion() {
        return fechaGraduacion;
    }
    public void setFechaGraduacion(LocalDate fechaGraduacion) {
        this.fechaGraduacion = fechaGraduacion;
    }
    public String getNumeroLicenciaMedica() {
        return numeroLicenciaMedica;
    }
    public void setNumeroLicenciaMedica(String numeroLicenciaMedica) {
        this.numeroLicenciaMedica = numeroLicenciaMedica;
    }
    public String getEspecialidad() {
        return especialidad;
    }
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }
    public String getSector() {
        return sector;
    }
    public void setSector(String sector) {
        this.sector = sector;
    }
    public String getTurnoGuardia() {
        return turnoGuardia;
    }
    public void setTurnoGuardia(String turnoGuardia) {
        this.turnoGuardia = turnoGuardia;
    }
    public LocalTime getHorasGuardia() {
        return horasGuardia;
    }
    public void setHorasGuardia(LocalTime horasGuardia) {
        this.horasGuardia = horasGuardia;
    }
    public String getPacienteAsignado() {
        return pacienteAsignado;
    }
    public void setPacienteAsignado(String pacienteAsignado) {
        this.pacienteAsignado = pacienteAsignado;
    }
    public String getTratamiento() {
        return tratamiento;
    }
    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }
    public String getReceta() {
        return receta;
    }
    public void setReceta(String receta) {
        this.receta = receta;
    }
    
    public void darAlta(Doctor doctor) throws ElementoDuplicadoException{
        if(listaDoctor.contains(doctor)) {
            throw new ElementoDuplicadoException("Los datos del doctor ya se encuentra en la lista");            
        } else {
            listaDoctor.add(doctor);
            System.out.println("Doctor dado de alta exitosamente");
        }
    }
    
    public Doctor buscarDoctor(Doctor doctorBuscar) throws ElementoInexistenteException{
        for(Doctor doc : listaDoctor) {
            if(doc.equals(doctorBuscar) == true) {
                System.out.println("Se encontro el doctor buscado");
                return doc;
            }
        }
        throw new ElementoInexistenteException("No se encontro la persona buscada");
    }
    
    public void modificarDoctor(Doctor docBuscar, Doctor docActualizado) {
        try {
            int indice = listaDoctor.indexOf(this.buscarDoctor(docBuscar));
            listaDoctor.set(indice, docActualizado);
            System.out.println("Los datos del doctor fue actualizado exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
     
    public void darBaja(Doctor medico) throws ElementoDuplicadoException{
        if(listaDoctor.contains(medico)) {
            listaDoctor.remove(medico);
            System.out.println("Doctor dado de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("Los datos del doctor no se encuentra en la lista");            
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de Doctores:");        
        for(Doctor doc : listaDoctor) {
            System.out.println(doc);
        }
    }
    
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Doctor doctor = (Doctor) obj;
        return doctor.numeroLicenciaMedica.equals(this.numeroLicenciaMedica) && doctor.especialidad.equals(this.especialidad);
    }
}