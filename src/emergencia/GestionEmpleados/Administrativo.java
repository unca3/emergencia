/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionEmpleados;
import emergencia.GestionAsistenciaMedica.SolicitudAsistencia;
import java.util.List;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 *
 * @author Julian
 */
public class Administrativo extends Empleado{
    private String educacion;
    private String puesto;
    private String supervisor;
    private Boolean accesoRegistros;
   
    private final List<Administrativo> listaAdministrativo = new ArrayList<>();

    public Administrativo(String educacion, String puesto, String supervisor, Boolean accesoRegistros, List<SolicitudAsistencia> solicitudesAsistencia, LocalDate fechaContratacion, Integer añosExperiencia, LocalTime horarioTrabajo, Double salario, String beneficios, LocalTime horasTrabajadas, String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        super(fechaContratacion, añosExperiencia, horarioTrabajo, salario, beneficios, horasTrabajadas, nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.educacion = educacion;
        this.puesto = puesto;
        this.supervisor = supervisor;
        this.accesoRegistros = accesoRegistros;
    }

    public String getEducacion() {
        return educacion;
    }
    public String getPuesto() {
        return puesto;
    }
    public String getSupervisor() {
        return supervisor;
    }
    public Boolean getAccesoRegistros() {
        return accesoRegistros;
    }

    public void setEducacion(String educacion) {
        this.educacion = educacion;
    }
    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }
    public void setSupervisor(String supervisor) {
        this.supervisor = supervisor;
    }
    public void setAccesoRegistros(Boolean accesoRegistros) {
        this.accesoRegistros = accesoRegistros;
    }
    
    public void darAlta(Administrativo administrativo) throws ElementoDuplicadoException{
        if(listaAdministrativo.contains(administrativo)) {
            throw new ElementoDuplicadoException("La persona ya se encuentra en la lista");            
        } else {
            listaAdministrativo.add(administrativo);
            System.out.println("Movil dado de alta exitosamente");
        }
    }
    
    public Administrativo buscarAdministrativo(Administrativo adminBuscar) throws ElementoInexistenteException{
        for(Administrativo admin : listaAdministrativo) {
            if(admin.equals(adminBuscar) == true) {
                System.out.println("Se encontro la persona administrativa buscada");
                return admin;
            }
        }
        throw new ElementoInexistenteException("No se encontro la persona buscada");
    }
    
    public void modificarAdministrativo(Administrativo adminBuscar, Administrativo adminActualizado) {
        try {
            int indice = listaAdministrativo.indexOf(this.buscarAdministrativo(adminBuscar));
            listaAdministrativo.set(indice, adminActualizado);
            System.out.println("El administrativo fue actualizado exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
     
    public void darBaja(Administrativo admin) throws ElementoInexistenteException{
        if(listaAdministrativo.contains(admin)) {
            listaAdministrativo.remove(admin);
            System.out.println("Administador dado de baja exitosamente");
        } else {
            throw new ElementoInexistenteException("El empleado administrativo no se encuentra en la lista");            
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de administradores:");        
        for(Administrativo admin : listaAdministrativo) {
            System.out.println(admin);
        }
    }
    
    @Override
    public boolean equals(Object obj) {
        if(this == obj) {
            return true;
        }
        if(obj == null || getClass() != obj.getClass()) {
            return false;
        }
        Administrativo administrativo = (Administrativo)obj;
        return administrativo.getNroDocumento().equals(this.getNroDocumento()) && administrativo.getNombreApellido().equals(this.getNombreApellido()) && administrativo.getPuesto().equals(this.getPuesto());
    }
}
