/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia;
import emergencia.GestionAfiliado.RegistroAfiliado;
import emergencia.GestionAfiliado.RegistroFamiliar;
import emergencia.GestionPagos.RegistroPagos;

/**
 *
 * @author Julian
 */
public class Emergencia {
    private static Emergencia emergencia;
    
    RegistroAfiliado registroAfiliado = new RegistroAfiliado();
    RegistroFamiliar registroFamiliar = new RegistroFamiliar();
    RegistroPagos registroPagos = new RegistroPagos();

    private Emergencia() {
    }
    
    public static Emergencia instancia() {
        if(emergencia == null) {
            
            emergencia = new Emergencia();
        }
        return emergencia;
    }
    public RegistroAfiliado getRegistroAfiliado() {
        return registroAfiliado;
    }
    public RegistroFamiliar getRegistroFamiliar() {
        return registroFamiliar;
    }
    public RegistroPagos getRegistroPagos() {
        return registroPagos;
    }
}
