/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package emergencia.Interfaz.gestionAfiliado;

import emergencia.Emergencia;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import emergencia.GestionAfiliado.Afiliado;
import emergencia.GestionAfiliado.Familiar;
import emergencia.GestionAfiliado.RegistroAfiliado;
import emergencia.GestionAfiliado.RegistroFamiliar;
import emergencia.Interfaz.InterfazMenuPrincipal;
import emergencia.Interfaz.gestionPagos.InterfazGestionPagos;
import java.time.LocalDate;
import java.time.Month;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Usuario
 */
public class InterfazGestionAfiliado extends javax.swing.JFrame {
    private final RegistroAfiliado registro = Emergencia.instancia().getRegistroAfiliado();
    private final RegistroFamiliar registroF = Emergencia.instancia().getRegistroFamiliar();
    
    private DefaultTableModel modeloTablaAfiliados = new DefaultTableModel(new String[]{"Nombre","Documento","Sexo","Edad","Domicilio","Fecha de nacimiento","Correo","Cuenta","Fecha de afiliacion"},registro.buscarTodo().size());
    private DefaultTableModel modeloTablaFamiliares = new DefaultTableModel(new String[]{"Nombre","Documento","Sexo","Edad","Domicilio","Fecha de nacimiento","Afiliado titular"},registroF.buscarTodo().size());
    private Afiliado afiliadoBuscado;
    /**
     * Creates new form InterfazGestionAfiliado
     */
    public InterfazGestionAfiliado() {
        initComponents();
        this.setLocationRelativeTo(null);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new javax.swing.JPanel();
        jPanel1 = new javax.swing.JPanel();
        JBtnAgregar = new javax.swing.JButton();
        JBtnMostrar = new javax.swing.JButton();
        JBtnEliminar = new javax.swing.JButton();
        JBtnPagar = new javax.swing.JButton();
        jBtnVolver = new javax.swing.JButton();
        JBtnLimpiar = new javax.swing.JButton();
        jBtnModificar = new javax.swing.JButton();
        jBtnAgregarFamiliar = new javax.swing.JButton();
        jBtnMostrarFa = new javax.swing.JButton();
        JLblTitulo = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTableMostrarAfiliados = new javax.swing.JTable();
        jPanel3 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jTxtNombre = new javax.swing.JTextField();
        jTxtDocumento = new javax.swing.JTextField();
        jTxtNroCuenta = new javax.swing.JTextField();
        jTxtFechaAfiliacion = new javax.swing.JTextField();
        jBtnLimpiar = new javax.swing.JButton();
        JBtnBuscar = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTableMostrarFamiliar = new javax.swing.JTable();

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 100, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        JBtnAgregar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        JBtnAgregar.setText("AGREGAR");
        JBtnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBtnAgregarActionPerformed(evt);
            }
        });

        JBtnMostrar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        JBtnMostrar.setText("MOSTRAR LISTA");
        JBtnMostrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBtnMostrarActionPerformed(evt);
            }
        });

        JBtnEliminar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        JBtnEliminar.setText("ELIMINAR");
        JBtnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBtnEliminarActionPerformed(evt);
            }
        });

        JBtnPagar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        JBtnPagar.setText("VERIFICAR PAGOS");
        JBtnPagar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBtnPagarActionPerformed(evt);
            }
        });

        jBtnVolver.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBtnVolver.setText("VOLVER");
        jBtnVolver.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnVolverActionPerformed(evt);
            }
        });

        JBtnLimpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        JBtnLimpiar.setText("LIMPIAR TABLA");
        JBtnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBtnLimpiarActionPerformed(evt);
            }
        });

        jBtnModificar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBtnModificar.setText("MODIFICAR");
        jBtnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnModificarActionPerformed(evt);
            }
        });

        jBtnAgregarFamiliar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBtnAgregarFamiliar.setText("AGREGAR FAMILIAR");
        jBtnAgregarFamiliar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnAgregarFamiliarActionPerformed(evt);
            }
        });

        jBtnMostrarFa.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBtnMostrarFa.setText("MOSTRAR FAMILIARES");
        jBtnMostrarFa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnMostrarFaActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jBtnModificar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBtnAgregar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBtnPagar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBtnMostrar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 226, Short.MAX_VALUE)
                    .addComponent(JBtnEliminar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnVolver, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(JBtnLimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnAgregarFamiliar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jBtnMostrarFa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(JBtnAgregar)
                .addGap(30, 30, 30)
                .addComponent(JBtnMostrar)
                .addGap(30, 30, 30)
                .addComponent(JBtnLimpiar)
                .addGap(30, 30, 30)
                .addComponent(JBtnEliminar)
                .addGap(30, 30, 30)
                .addComponent(jBtnModificar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jBtnMostrarFa)
                .addGap(32, 32, 32)
                .addComponent(jBtnAgregarFamiliar)
                .addGap(30, 30, 30)
                .addComponent(JBtnPagar)
                .addGap(30, 30, 30)
                .addComponent(jBtnVolver)
                .addGap(10, 10, 10))
        );

        JLblTitulo.setFont(new java.awt.Font("Arial Black", 2, 18)); // NOI18N
        JLblTitulo.setText("GESTION DE AFILIADOS");

        jTableMostrarAfiliados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableMostrarAfiliados.setToolTipText("");
        jTableMostrarAfiliados.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMostrarAfiliadosMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(jTableMostrarAfiliados);

        jLabel2.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel2.setText("NOMBRE Y APELLIDO");

        jLabel3.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel3.setText("NRO DE DOCUMENTO");

        jLabel9.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel9.setText("NRO DE CUENTA");

        jLabel10.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jLabel10.setText("FECHA DE AFILIACION");

        jTxtNombre.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jTxtNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtNombreActionPerformed(evt);
            }
        });

        jTxtDocumento.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jTxtDocumento.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jTxtDocumentoActionPerformed(evt);
            }
        });

        jTxtNroCuenta.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jTxtFechaAfiliacion.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N

        jBtnLimpiar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        jBtnLimpiar.setText("LIMPIAR");
        jBtnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBtnLimpiarActionPerformed(evt);
            }
        });

        JBtnBuscar.setFont(new java.awt.Font("Arial", 0, 14)); // NOI18N
        JBtnBuscar.setText("BUSCAR");
        JBtnBuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JBtnBuscarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel9, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(174, 174, 174))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGap(147, 147, 147))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBtnLimpiar, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jTxtNroCuenta, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                        .addComponent(jTxtFechaAfiliacion, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(jTxtNombre, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 300, Short.MAX_VALUE)
                        .addComponent(jTxtDocumento, javax.swing.GroupLayout.Alignment.TRAILING))
                    .addComponent(JBtnBuscar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jTxtNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(jTxtDocumento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(jTxtNroCuenta, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel10)
                    .addComponent(jTxtFechaAfiliacion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 29, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JBtnBuscar)
                    .addComponent(jBtnLimpiar))
                .addContainerGap())
        );

        jTableMostrarFamiliar.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jTableMostrarFamiliar.setToolTipText("");
        jTableMostrarFamiliar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTableMostrarFamiliarMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(jTableMostrarFamiliar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(JLblTitulo, javax.swing.GroupLayout.PREFERRED_SIZE, 249, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE)
                            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 692, Short.MAX_VALUE))))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JLblTitulo, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 215, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jBtnVolverActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnVolverActionPerformed
        InterfazMenuPrincipal principal = new InterfazMenuPrincipal();
        principal.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_jBtnVolverActionPerformed

    private void JBtnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBtnAgregarActionPerformed
        InterfazAgregarAfiliado agregar = new InterfazAgregarAfiliado();
        agregar.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_JBtnAgregarActionPerformed

    private void JBtnMostrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBtnMostrarActionPerformed
        limpiarTabla();
//        try {
//            registro.darAlta(new Afiliado("julian@","111-222",LocalDate.of(2023,10,10),"julian","111","m",22,"casa2",LocalDate.of(2001,10,10)));
//        } catch (ElementoDuplicadoException ex) {
//            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
//        }
        //limpiarTabla();
        modeloTablaAfiliados.setRowCount(registro.buscarTodo().size());
        
        for(int i = 0; i < registro.buscarTodo().size(); i++) {
            Afiliado afiliado = (Afiliado)registro.buscarTodo().get(i);
            modeloTablaAfiliados.setValueAt(afiliado.getNombreApellido(), i, 0);
            modeloTablaAfiliados.setValueAt(afiliado.getNroDocumento(), i, 1);
            modeloTablaAfiliados.setValueAt(afiliado.getSexo(), i, 2);
            modeloTablaAfiliados.setValueAt(afiliado.getEdad(), i, 3);
            modeloTablaAfiliados.setValueAt(afiliado.getDomicilio(), i, 4);
            modeloTablaAfiliados.setValueAt(afiliado.getFechaNacimiento(), i, 5);
            modeloTablaAfiliados.setValueAt(afiliado.getCorreo(), i, 6);
            modeloTablaAfiliados.setValueAt(afiliado.getNroCuenta(), i, 7);
            modeloTablaAfiliados.setValueAt(afiliado.getFechaAfiliacion(), i, 8);
        }
        jTableMostrarAfiliados.setModel(modeloTablaAfiliados);
    }//GEN-LAST:event_JBtnMostrarActionPerformed

    private void JBtnBuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBtnBuscarActionPerformed
        try {
            if((jTxtNroCuenta.getText().equals("") && jTxtNombre.getText().equals("") && jTxtDocumento.getText().equals("") && jTxtFechaAfiliacion.getText().equals("")) == true) {
                this.afiliadoBuscado = null;
                throw new ElementoInexistenteException("No se han ingresado todos los datos. Intente de nuevo");
            }
            
            limpiarTabla();
            limpiarTablaF();
            Afiliado buscar = new Afiliado("",jTxtNroCuenta.getText(),LocalDate.parse(jTxtFechaAfiliacion.getText()),jTxtNombre.getText(),jTxtDocumento.getText(),"", 7,"",LocalDate.of(2000, 10, 10));
            this.afiliadoBuscado = registro.buscarAfiliado(buscar);
            modeloTablaAfiliados.setRowCount(1);

            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getNombreApellido(), 0, 0);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getNroDocumento(), 0, 1);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getSexo(), 0, 2);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getEdad(), 0, 3);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getDomicilio(), 0, 4);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getFechaNacimiento(), 0, 5);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getCorreo(), 0, 6);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getNroCuenta(), 0, 7);
            modeloTablaAfiliados.setValueAt(afiliadoBuscado.getFechaAfiliacion(), 0, 8);
        } catch (ElementoInexistenteException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        }
        jTableMostrarAfiliados.setModel(modeloTablaAfiliados);
    }//GEN-LAST:event_JBtnBuscarActionPerformed

    private void jTxtNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtNombreActionPerformed

    private void jTxtDocumentoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jTxtDocumentoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jTxtDocumentoActionPerformed

    private void jBtnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnLimpiarActionPerformed
        limpiar();
    }//GEN-LAST:event_jBtnLimpiarActionPerformed

    private void JBtnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBtnLimpiarActionPerformed
        limpiarTabla();
        limpiarTablaF();
    }//GEN-LAST:event_JBtnLimpiarActionPerformed

    private void JBtnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBtnEliminarActionPerformed
        try {
            registro.darBaja(this.afiliadoBuscado);
            modeloTablaAfiliados.setRowCount(registro.buscarTodo().size());
            limpiarTabla();
            
            JOptionPane.showMessageDialog(rootPane, "Afiliado eliminado exitosamente");
        } catch (ElementoDuplicadoException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        }
    }//GEN-LAST:event_JBtnEliminarActionPerformed

    private void jTableMostrarAfiliadosMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMostrarAfiliadosMouseClicked
//        int seleccion = jTableMostrarAfiliados.rowAtPoint(evt.getPoint());
//
//        String nombre = (String) jTableMostrarAfiliados.getValueAt(seleccion, 0);
//        String documento =(String) jTableMostrarAfiliados.getValueAt(seleccion, 1);
//        String cuenta = (String) jTableMostrarAfiliados.getValueAt(seleccion, 7);
//        String fechaAfiliacion = (String) jTableMostrarAfiliados.getValueAt(seleccion, 8);
//
//        Afiliado buscar = new Afiliado("",cuenta,LocalDate.parse(fechaAfiliacion),nombre,documento,"", 7,"",LocalDate.of(2000, 10, 10));;
//
//        try{
//            this.afiliadoBuscado = registro.buscarAfiliado(buscar);
//        } catch (ElementoInexistenteException ex) {
//            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
//        }
    }//GEN-LAST:event_jTableMostrarAfiliadosMouseClicked

    private void jBtnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnModificarActionPerformed
        try {
            if(this.afiliadoBuscado == null) {
                throw new NullPointerException("Debe elegir un afilido. Presione buscar");
            }
            InterfazAgregarAfiliado principal = new InterfazAgregarAfiliado();
            principal.setVisible(true);
            this.dispose();

            Afiliado modificado = (Afiliado)registro.buscarTodo().get(registro.buscarTodo().size() - 1);
            registro.modificarAfiliado(this.afiliadoBuscado, modificado);
//            registro.darBaja(modificado);
            
            modeloTablaAfiliados.setRowCount(registro.buscarTodo().size());
            limpiarTabla();
//            JOptionPane.showMessageDialog(rootPane, "Afiliado modificado exitosamente");
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        } catch (ElementoInexistenteException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        }
    }//GEN-LAST:event_jBtnModificarActionPerformed

    private void jBtnAgregarFamiliarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnAgregarFamiliarActionPerformed
        try {
            if(afiliadoBuscado == null) {
                throw new NullPointerException("No se a seleccionado un afiliado. Presione buscar");
            }
            InterfazAgregarFamiliar principal = new InterfazAgregarFamiliar(afiliadoBuscado);
            principal.setVisible(true);
            this.dispose();
            
            //Familiar ultimo = registroF.getUltimoFamiliar();
            //registroF.buscarFamiliar(ultimo).setTitular(afiliadoBuscado);
            //registro.buscarAfiliado(afiliadoBuscado).setRegistroFamiliar(registroF);
            //JOptionPane.showMessageDialog(rootPane, "Familiar agregado exitosamente");
        } catch (NullPointerException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        }
    }//GEN-LAST:event_jBtnAgregarFamiliarActionPerformed

    private void jTableMostrarFamiliarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTableMostrarFamiliarMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jTableMostrarFamiliarMouseClicked

    private void jBtnMostrarFaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBtnMostrarFaActionPerformed
        try{
            if(afiliadoBuscado == null) {
                throw new ElementoInexistenteException("No se han asociado familiares al afiliado");
            }
            limpiarTablaF();
            modeloTablaFamiliares.setRowCount(registroF.buscarTodo().size());
            
            for(int i = 0; i < registroF.buscarTodo().size(); i++) {
                Familiar familiar = (Familiar)registroF.buscarTodo().get(i);
                if(familiar.getTitular().equals(afiliadoBuscado)) {
                    modeloTablaFamiliares.setValueAt(familiar.getNombreApellido(), i, 0);
                    modeloTablaFamiliares.setValueAt(familiar.getNroDocumento(), i, 1);
                    modeloTablaFamiliares.setValueAt(familiar.getSexo(), i, 2);
                    modeloTablaFamiliares.setValueAt(familiar.getEdad(), i, 3);
                    modeloTablaFamiliares.setValueAt(familiar.getDomicilio(), i, 4);
                    modeloTablaFamiliares.setValueAt(familiar.getFechaNacimiento(), i, 5);
                }
                //modeloTablaFamiliares.setValueAt((String)familiar.getTitular().getNombreApellido(), i, 6);
                //System.out.println(familiar.getTitular().getNombreApellido());
            }
            jTableMostrarFamiliar.setModel(modeloTablaFamiliares);
        } catch (ElementoInexistenteException ex) {
            JOptionPane.showMessageDialog(rootPane, ex.getMessage(), "Alerta",0);
        }
    }//GEN-LAST:event_jBtnMostrarFaActionPerformed

    private void JBtnPagarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_JBtnPagarActionPerformed
        try{
            if(afiliadoBuscado == null) {
                throw new NullPointerException("No se ha elegido un afiliado. Presione buscar");
            }
            InterfazGestionPagos principal = new InterfazGestionPagos();
            principal.setAfiliado(afiliadoBuscado);
            principal.setVisible(true);
            this.dispose();
        } catch (NullPointerException e) {
            JOptionPane.showMessageDialog(rootPane, e.getMessage(), "Alerta",0);            
        }
    }//GEN-LAST:event_JBtnPagarActionPerformed
    private void limpiarTabla() {
        modeloTablaAfiliados.setRowCount(0);
    }
    private void limpiarTablaF() {
        modeloTablaFamiliares.setRowCount(0);        
    }
    private void limpiar() {
        jTxtNombre.setText("");
        jTxtDocumento.setText("");
        jTxtNroCuenta.setText("");
        jTxtFechaAfiliacion.setText("");
    }
    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(InterfazGestionAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(InterfazGestionAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(InterfazGestionAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(InterfazGestionAfiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new InterfazGestionAfiliado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton JBtnAgregar;
    private javax.swing.JButton JBtnBuscar;
    private javax.swing.JButton JBtnEliminar;
    private javax.swing.JButton JBtnLimpiar;
    private javax.swing.JButton JBtnMostrar;
    private javax.swing.JButton JBtnPagar;
    private javax.swing.JLabel JLblTitulo;
    private javax.swing.JButton jBtnAgregarFamiliar;
    private javax.swing.JButton jBtnLimpiar;
    private javax.swing.JButton jBtnModificar;
    private javax.swing.JButton jBtnMostrarFa;
    private javax.swing.JButton jBtnVolver;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTableMostrarAfiliados;
    private javax.swing.JTable jTableMostrarFamiliar;
    private javax.swing.JTextField jTxtDocumento;
    private javax.swing.JTextField jTxtFechaAfiliacion;
    private javax.swing.JTextField jTxtNombre;
    private javax.swing.JTextField jTxtNroCuenta;
    // End of variables declaration//GEN-END:variables
}
