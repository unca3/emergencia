/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionMoviles;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroMoviles {
    private final List<Movil> listaMoviles = new ArrayList<>();

    public void darAlta(Movil movil) throws ElementoDuplicadoException{
        if(listaMoviles.contains(movil)) {
            throw new ElementoDuplicadoException("El movil ya se encuentra en la lista");
        } else {
            listaMoviles.add(movil);
            System.out.println("Movil dado de alta exitosamente");
        }
    }
    public Movil buscarMovil(Movil movilBuscar) throws ElementoInexistenteException{
        for(Movil mov : listaMoviles) {
            if(mov.equals(movilBuscar) == true) {
                System.out.println("Se encontro el movil buscado");
                return mov;
            }
        }
        throw new ElementoInexistenteException("No se encontro el afiliado buscado");
    }
    public void modificarMoviles(Movil movilBuscado, Movil movilActualizado) {
        try {
            int indice = listaMoviles.indexOf(this.buscarMovil(movilBuscado));
            listaMoviles.set(indice, movilActualizado);
            System.out.println("Movil fue modificado exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void darBaja(Movil movil) throws ElementoDuplicadoException{
        if(listaMoviles.contains(movil)) {
            listaMoviles.remove(movil);
            System.out.println("Moviles dado de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("El movil no se encuentra en la lista");
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de Moviles:");        
        for(Movil mov : listaMoviles) {
            System.out.println(mov);
        }
    }    
}
