/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionMoviles;
import java.util.List;
import emergencia.GestionEmpleados.Chofer;
import emergencia.GestionEmpleados.Enfermero;
import emergencia.GestionAsistenciaMedica.SolicitudAsistencia;
/**
 *
 * @author Usuario
 */
public class Movil {
    private List <Chofer> listaChoferes;
    private List <Enfermero> listaEnfermero;
    private List <SolicitudAsistencia> listaAsistencia;

    private String marca;
    private String tipo;
    private String modelo;
    private String patente;
    private int chasis;
    private int traccion;

    public Movil(List<Chofer> listaChoferes, List<Enfermero> listaEnfermero, List<SolicitudAsistencia> listaAsistencia, String marca, String tipo, String modelo, String patente, int chasis, int traccion) {
        this.listaChoferes = listaChoferes;
        this.listaEnfermero = listaEnfermero;
        this.listaAsistencia = listaAsistencia;
        this.marca = marca;
        this.tipo = tipo;
        this.modelo = modelo;
        this.patente = patente;
        this.chasis = chasis;
        this.traccion = traccion;
    }

    public List<Chofer> getListaChoferes() {
        return listaChoferes;
    }

    public void setListaChoferes(List<Chofer> listaChoferes) {
        this.listaChoferes = listaChoferes;
    }

    public List<Enfermero> getListaEnfermero() {
        return listaEnfermero;
    }

    public void setListaEnfermero(List<Enfermero> listaEnfermero) {
        this.listaEnfermero = listaEnfermero;
    }

    public List<SolicitudAsistencia> getListaAsistencia() {
        return listaAsistencia;
    }

    public void setListaAsistencia(List<SolicitudAsistencia> listaAsistencia) {
        this.listaAsistencia = listaAsistencia;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getPatente() {
        return patente;
    }

    public void setPatente(String patente) {
        this.patente = patente;
    }

    public int getChasis() {
        return chasis;
    }

    public void setChasis(int chasis) {
        this.chasis = chasis;
    }

    public int getTraccion() {
        return traccion;
    }

    public void setTraccion(int traccion) {
        this.traccion = traccion;
    }    
}