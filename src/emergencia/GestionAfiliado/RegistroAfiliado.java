/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAfiliado;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroAfiliado {
    private final List<Afiliado> listaAfiliado = new ArrayList<>();

    public void darAlta(Afiliado afiliado) throws ElementoDuplicadoException{
        if(listaAfiliado.contains(afiliado)) {
            throw new ElementoDuplicadoException("El afiliado ya se encuentra en la lista");
        } else {
            listaAfiliado.add(afiliado);
            System.out.println("Afiliado dado de alta exitosamente");            
        }
    }
    public Afiliado buscarAfiliado(Afiliado afiliadoBuscado) throws ElementoInexistenteException{
        for(Afiliado afi : listaAfiliado) {
            if(afi.equals(afiliadoBuscado) == true) {
                System.out.println("Se encontro el afiliado buscado");
                return afi;
            }
        }
        throw new ElementoInexistenteException("No se encontro el afiliado buscado");
    }
    public ArrayList buscarTodo() {
        return (ArrayList) listaAfiliado;
    }
    public void modificarAfiliado(Afiliado afiliadoBuscado, Afiliado afiliadoActualizado) throws ElementoInexistenteException {
            int indice = listaAfiliado.indexOf(this.buscarAfiliado(afiliadoBuscado));
        if(indice == -1) {            
            throw new ElementoInexistenteException("El afiliado no se encuentra en la lista");
        } else {
            listaAfiliado.set(indice, afiliadoActualizado);
            listaAfiliado.remove(afiliadoBuscado);
            System.out.println("Afiliado modificado exitosamente");
        }
    }
    public void darBaja(Afiliado afiliado) throws ElementoDuplicadoException{
        if(listaAfiliado.contains(afiliado)) {
            listaAfiliado.remove(afiliado);
            System.out.println("Afiliado dado de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("El afiliado no se encuentra en la lista");
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de afiliados:");        
        for(Afiliado afi : listaAfiliado) {
            System.out.println(afi);
        }
    }
}