/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAfiliado;
import emergencia.Excepciones.ElementoDuplicadoException;
import emergencia.Excepciones.ElementoInexistenteException;
import java.util.ArrayList;
import java.util.List;
/**
 *
 * @author Julian
 */
public class RegistroFamiliar {
    private final List <Familiar> listaFamiliar = new ArrayList<>();
    
    public void darAlta(Familiar familiar) throws ElementoDuplicadoException{
        if(listaFamiliar.contains(familiar)) {
            throw new ElementoDuplicadoException("El familiar ya se encuentra en la lista");
        } else {
            listaFamiliar.add(familiar);
            System.out.println("Familiar dado de alta exitosamente");
        }        
    }
    public Familiar buscarFamiliar(Familiar familiarBuscado) throws ElementoInexistenteException{
        for(Familiar fami : listaFamiliar) {
            if(fami.equals(familiarBuscado) == true) {
                System.out.println("Se encontro el familiar buscado");
                return fami;
            }
        }
        throw new ElementoInexistenteException("No se encontro el familiar buscado");
    }
    public ArrayList buscarTodo() {
        return (ArrayList) listaFamiliar;
    } 
    public void modificarFamiliar(Familiar familiarBuscado, Familiar familiarActualizado) {
        try {
            int indice = listaFamiliar.indexOf(this.buscarFamiliar(familiarBuscado));
            listaFamiliar.set(indice, familiarActualizado);
            System.out.println("Familiar modificado exitosamente");
        } catch (ElementoInexistenteException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public void darBaja(Familiar familiar) throws ElementoDuplicadoException{
        if(listaFamiliar.contains(familiar)) {
            listaFamiliar.remove(familiar);
            System.out.println("Familiar dado de baja exitosamente");
        } else {
            throw new ElementoDuplicadoException("El familiar no se encuentra en la lista");
        }
    }
    public void mostrarLista() {
        System.out.println("Lista de familiares asociados al afiliado:");        
        for(Familiar fami : listaFamiliar) {
            System.out.println(fami);
        }
    }
    public int getCantidadFamiliares() {
        return listaFamiliar.size();
    }
    public Familiar getUltimoFamiliar() throws ElementoInexistenteException {
        if (!listaFamiliar.isEmpty()) {
            return listaFamiliar.get(listaFamiliar.size() - 1);
        } else {
            throw new ElementoInexistenteException("No se ha agregado ningun familiar");
        }
    }
}
