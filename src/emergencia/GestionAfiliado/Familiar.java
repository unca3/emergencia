/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAfiliado;
import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class Familiar extends Persona{
    private Afiliado titular;

    public Familiar(String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento, Afiliado afiliadoTitular) {
        super(nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.titular = afiliadoTitular;
    }

    public Afiliado getTitular() {
        return titular;
    }
    public void setTitular(Afiliado titular) {
        this.titular = titular;
    }
    
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Familiar flia = (Familiar) obj;
        return flia.getNombreApellido().equals(this.getNombreApellido()) && flia.getNroDocumento().equals(this.getNroDocumento());
    }
    @Override
    public String toString() {
        return "Familiar{" + "nombreApellido = " + this.getNombreApellido() + ", nroDocumento = " + this.getNroDocumento() + ", sexo = " + this.getSexo() + ", edad = " + this.getEdad() + ", domicilio = " + this.getDomicilio() + ", fechaNacimiento = " + this.getFechaNacimiento() + '}';
    }
}