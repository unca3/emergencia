/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAfiliado;
import java.time.LocalDate;

/**
 *
 * @author Usuario
 */
public class Persona {
    private String nombreApellido;
    private String nroDocumento;
    private String sexo;
    private int edad;
    private String domicilio;
    private LocalDate fechaNacimiento;
    
    public Persona(String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        this.nombreApellido = nombreApellido;
        this.nroDocumento = nroDocumento;
        this.sexo = sexo;
        this.edad = edad;
        this.domicilio = domicilio;
        this.fechaNacimiento = fechaNacimiento;
    }
    
    public String getNombreApellido() {
        return nombreApellido;
    }
    public void setNombreApellido(String nombreApellido) {
        this.nombreApellido = nombreApellido;
    }
    public String getNroDocumento() {
        return nroDocumento;
    }
    public void setNroDocumento(String nroDocumento) {
        this.nroDocumento = nroDocumento;
    }
    public String getSexo() {
        return sexo;
    }
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }
    public int getEdad() {
        return edad;
    }
    public void setEdad(int edad) {
        this.edad = edad;
    }
    public String getDomicilio() {
        return domicilio;
    }
    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }
    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }
    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }
    
    @Override
    public boolean equals(Object obj){
        if(this == obj){
            return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Persona persona = (Persona) obj;
        return persona.nroDocumento.equals(this.nroDocumento) && persona.nombreApellido.equals(this.nombreApellido);
    }
}
