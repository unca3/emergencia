/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package emergencia.GestionAfiliado;
import java.time.LocalDate;
/**
 *
 * @author Usuario
 */
public class Afiliado extends Persona{
    private String correo;
    private String nroCuenta;
    private LocalDate fechaAfiliacion;
    private RegistroFamiliar registroFamiliares;

    public Afiliado(String correo, String nroCuenta, LocalDate fechaAfiliacion, String nombreApellido, String nroDocumento, String sexo, int edad, String domicilio, LocalDate fechaNacimiento) {
        super(nombreApellido, nroDocumento, sexo, edad, domicilio, fechaNacimiento);
        this.correo = correo;
        this.nroCuenta = nroCuenta;
        this.fechaAfiliacion = fechaAfiliacion;
    }

    public String getCorreo() {
        return correo;
    }
    public String getNroCuenta() {
        return nroCuenta;
    }
    public LocalDate getFechaAfiliacion() {
        return fechaAfiliacion;
    }
    public void setCorreo(String correo) {
        this.correo = correo;
    }
    public void setNroCuenta(String nroCuenta) {
        this.nroCuenta = nroCuenta;
    }
    public void setFechaAfiliacion(LocalDate fechaAfiliacion) {
        this.fechaAfiliacion = fechaAfiliacion;
    }
    
    public void getRegistroFamiliar() {
        System.out.println("Afiliado titular:");        
        System.out.println(this.toString());
        registroFamiliares.mostrarLista();
    }
    public void setRegistroFamiliar(RegistroFamiliar registroFamiliar) {
        this.registroFamiliares = registroFamiliar;
    }
    public int getCantidadFamiliares() {
        return this.registroFamiliares.getCantidadFamiliares();
    }
    
    @Override
    public boolean equals(Object obj){
        if(this == obj){
          return true;
        }
        if(obj == null || getClass() != obj.getClass()){
            return false;
        }
        Afiliado afiliado = (Afiliado) obj;
        return afiliado.getNombreApellido().equals(this.getNombreApellido()) && afiliado.getNroDocumento().equals(this.getNroDocumento()) && afiliado.nroCuenta.equals(this.nroCuenta) && afiliado.getFechaAfiliacion().equals(this.getFechaAfiliacion());
    }
    
    @Override
    public String toString() {
        return "Afiliado{" + "nroCuenta = " + nroCuenta + ", fechaAfiliacion = " + fechaAfiliacion + ", nombreApellido = " + this.getNombreApellido() + ", nroDocumento = " + this.getNroDocumento() + ", sexo = " + this.getSexo() + ", edad = " + this.getEdad() + ", domicilio = " + this.getDomicilio() + ", fechaNacimiento = " + this.getFechaNacimiento() + '}';
    }
}